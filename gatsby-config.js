/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: "Simply Recipes",
    description: "Nice and Clean Project",
    author: "aku budak boy",
    person: {
      name: "nazri",
      age: "24",
    },
    simpleData: ["item1", "item2"],
    complexData: [
      {
        name: "tashah",
        age: "27",
      },
      {
        name: "wana",
        age: "25",
      },
    ],
  },
  plugins: [
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: ``,
        // Learn about environment variables: https://gatsby.dev/env-vars
        accessToken: ``,
      },
    },
  ],
}
