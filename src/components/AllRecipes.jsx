import React from "react"
import TagsList from "./TagsList"
import RecipesList from "./RecipesList"
import { useStaticQuery, graphql } from "gatsby"

const AllRecipes = () => {
  const {
    content: { nodes: recipes },
  } = useStaticQuery(graphql`
    {
      content: allContentfulRecipe {
        nodes {
          title
          prepTime
          cookTime
          content {
            tags
          }
          image {
            gatsbyImageData(layout: CONSTRAINED, placeholder: TRACED_SVG)
          }
        }
      }
    }
  `)

  return (
    <section className="recipes-container">
      <TagsList recipes={recipes}></TagsList>
      <RecipesList recipes={recipes}></RecipesList>
    </section>
  )
}

export default AllRecipes
