import React from "react"

const Footer = () => {
  return (
    <footer className="page-footer">
      <p>
        &copy; {new Date().getFullYear()} <span>Simply Recipes</span> Built With
        <span> Gatsby</span>
      </p>
    </footer>
  )
}

export default Footer
