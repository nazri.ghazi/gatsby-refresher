import React from "react"
import { graphql } from "gatsby"
import RecipesList from "../components/RecipesList"
import Layout from "../components/Layout"
const tagTemplate = props => {
  console.log(props)
  return (
    <Layout>
      <main className="page">
        <h2>{props.pageContext.tag}</h2>

        <div className="tag-recipes">
          <RecipesList
            recipes={props.data.allContentfulRecipe.nodes}
          ></RecipesList>
        </div>
      </main>
    </Layout>
  )
}

export const query = graphql`
  query getRecipe($tag: String) {
    allContentfulRecipe(filter: { content: { tags: { eq: $tag } } }) {
      nodes {
        prepTime
        cookTime
        id
        title
        image {
          gatsbyImageData(layout: CONSTRAINED, placeholder: TRACED_SVG)
        }
      }
    }
  }
`

export default tagTemplate
