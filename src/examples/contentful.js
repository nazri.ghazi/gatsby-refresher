import React from "react"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { BsClockHistory, BsClock, BsPeople } from "react-icons/bs"
import { Link } from "gatsby"
import Layout from "../components/Layout"
const RecipeTemplate = ({ data }) => {
  //   const {
  //     title,
  //     cookTime,
  //     description: { description },
  //     prepTime,
  //     servings,
  //     content,
  //     image,
  //   } = data.contentfulRecipe
  //   const { tags, instructions, ingredients, tools } = content
  return (
    <Layout>
      <main className="page">
        <div className="recipe-page">
          <section className="recipe-hero">
            <GatsbyImage
              image={getImage(image)}
              alt={title}
              className="about-img"
            ></GatsbyImage>
            <article className="recipe-info">
              {/* title */}
              <h2></h2>
              {/* desc */}
              <p>{description}</p>
              <div className="recipe-icons">
                <article>
                  <BsClockHistory></BsClockHistory>
                  <h5>Prep time</h5>
                  {/* prepTime */}
                  <p> min</p>
                </article>
                <article>
                  <BsPeople></BsPeople>
                  <h5>Servings </h5>
                  {/* servings */}
                  <p> people</p>
                </article>
                <article>
                  <BsClock></BsClock>
                  <h5>Cook time</h5>
                  {/* cook time */}
                  <p> min</p>
                </article>
              </div>
              <p className="recipe-tags">
                {/* tag link */}
                Tags :
                {/* {tags.map((tag, index) => {
                  return (
                    <Link to={`/${tag}`} key={index}>
                      {tag}
                    </Link>
                  )
                })} */}
              </p>
            </article>
          </section>
          <section className="recipe-content">
            <article>
              <h4>instructions</h4>
              {/* instruction */}
              {/* {instructions.map((instruction, index) => {
                return (
                  <div key={index} className="single-instruction">
                    <header>
                      <p>step {index + 1}</p>
                      <div></div>
                    </header>
                    <p>{instruction}</p>
                  </div>
                )
              })} */}
            </article>
            <article className="second-column">
              <h4>ingredients</h4>

              {/* ingredient */}

              {/* {ingredients.map((ingredient, index) => {
                return (
                  <div key={index} className="single-instruction">
                    <header>
                      <p>step {index + 1}</p>
                      <div></div>
                    </header>
                    <p>{ingredient}</p>
                  </div>
                )
              })} */}
            </article>
          </section>
        </div>
      </main>
    </Layout>
  )
}
// export const query = graphql`
//   query getSingleRecipe($title: String) {
//     contentfulRecipe(title: { eq: $title }) {
//       title
//       cookTime
//       description {
//         description
//       }
//       prepTime
//       servings
//       content {
//         ingredients
//         instructions
//         tags
//         tools
//       }
//       image {
//         gatsbyImageData
//       }
//     }
//   }
// `
export default RecipeTemplate
