import React from "react"
import { useStaticQuery, graphql } from "gatsby"

const query = graphql`
  {
    site {
      info: siteMetadata {
        author
        description
        simpleData
        title
        complexData {
          age
          name
        }
        person {
          name
        }
      }
    }
  }
`

const FetchData = () => {
  const {
    site: {
      info: { author, complexData },
    },
  } = useStaticQuery(query)

  return (
    <div className="">
      <h2 className="">{author}</h2>
      {complexData.map((item, index) => {
        return (
          <p key={index}>
            {item.name}, {item.age}
          </p>
        )
      })}
    </div>
  )
}

export default FetchData
