import React from "react"
import { Link, graphql } from "gatsby"
import setupTags from "../utils/setupTags"
import Layout from "../components/Layout"
const Tags = ({ data }) => {
  const newTags = setupTags(data.tagContent.nodes)
  return (
    <Layout>
      <main className="page">
        <section className="tags-page">
          {newTags.map((tag, index) => {
            const [text, value] = tag
            return (
              <Link key={index} to={`/${text}`} key="index" className="tag">
                <h5>{text}</h5>
                <p>
                  {value} {value < 2 ? `recipe` : `recipes`}
                </p>
              </Link>
            )
          })}
        </section>
      </main>
    </Layout>
  )
}

export const query = graphql`
  {
    tagContent: allContentfulRecipe(filter: {}) {
      nodes {
        content {
          tags
        }
      }
    }
  }
`

export default Tags
