import React from "react"
import Gallery from "../examples/gallery"
import Layout from "../components/Layout"

const Testing = props => {
  return (
    <Layout>
      <main className="page">
        <Gallery></Gallery>
      </main>
    </Layout>
  )
}

export default Testing
