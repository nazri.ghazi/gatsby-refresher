import React from "react"
import Layout from "../components/Layout"
import { graphql } from "gatsby"
import RecipesList from "../components/RecipesList"
const Contact = props => {
  console.log(props)
  return (
    <Layout>
      <div className="page">
        <section className="contact-page">
          <article className="contact-info">
            <h3>Want To Get In Touch?</h3>
            <p>
              I'm baby cred affogato selfies pour-over. 3 wolf moon seitan
              shaman selvage, paleo sartorial craft beer church-key raclette
              meditation heirloom marfa.
            </p>
            <p>
              Tote bag before they sold out polaroid try-hard yuccie lo-fi, +1
              chia yr copper mug glossier cronut tumblr everyday carry.
            </p>
            <p>Mustache stumptown YOLO cardigan ethical tousled letterpress.</p>
          </article>
          <article>
            <form className="form contact-form">
              <div className="form-row">
                <label htmlFor="name">your name</label>
                <input type="text" name="name" id="name" />
              </div>
              <div className="form-row">
                <label htmlFor="name">your email</label>
                <input type="text" name="email" id="name" />
              </div>
              <div className="form-row">
                <label htmlFor="name">message</label>
                <textarea type="text" name="message" id="name" />
              </div>
              <button type="submit" className="btn block">
                submit
              </button>
            </form>
          </article>
        </section>
        <section className="featured-recipes">
          <h5>Look at this Awesomesource!</h5>
          <RecipesList recipes={props.data.content.nodes}></RecipesList>
        </section>
      </div>
    </Layout>
  )
}

export const query = graphql`
  {
    content: allContentfulRecipe(filter: { featured: { eq: true } }) {
      nodes {
        title
        prepTime
        cookTime
        image {
          gatsbyImageData(layout: CONSTRAINED, placeholder: TRACED_SVG)
        }
        content {
          tags
        }
      }
    }
  }
`

export default Contact
