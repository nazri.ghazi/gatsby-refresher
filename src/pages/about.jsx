import React from "react"
import { Link } from "gatsby"
import Layout from "../components/Layout"
import { StaticImage } from "gatsby-plugin-image"
import { graphql } from "gatsby"
import RecipesList from "../components/RecipesList"
const About = ({ data }) => {
  // console.log(props)
  const recipes = data.allContentfulRecipe.nodes
  return (
    <Layout>
      <main className="page">
        <section className="about-page">
          <article>
            <h2>I'm baby cred affogato.</h2>
            <p>
              3 wolf moon seitan shaman selvage, paleo sartorial craft beer
              church-key raclette meditation heirloom marfa. Tote bag before
              they sold out polaroid try-hard yuccie lo-fi,
            </p>
            <p>
              Austin prism biodiesel, celiac man braid scenester blog kitsch
              knausgaard.
            </p>
            <Link to="/contact" className="btn">
              Contact
            </Link>
          </article>
          <StaticImage
            src="../assets/images/big.jpg"
            alt="Person pouring salt"
            className="about-img"
            placeholder="blurred"
          ></StaticImage>
        </section>
        <section className="featured-recipes">
          <h5>Look at this Awesomesource!</h5>
          <RecipesList recipes={recipes}></RecipesList>
        </section>
      </main>
    </Layout>
  )
}

export const query = graphql`
  {
    allContentfulRecipe(filter: { featured: { eq: true } }) {
      nodes {
        title
        prepTime
        servings
        id
        cookTime
        content {
          id
        }
        image {
          gatsbyImageData(placeholder: DOMINANT_COLOR, layout: CONSTRAINED)
        }
      }
    }
  }
`

export default About
