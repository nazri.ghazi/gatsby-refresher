import React from "react"
import { Link } from "gatsby"
import Layout from "../components/Layout"
import { StaticImage } from "gatsby-plugin-image"
import FetchData from "../examples/fetchData"
import AllRecipes from "../components/AllRecipes"
export default function Home() {
  return (
    <div>
      <Layout>
        <main className="page">
          <header className="hero">
            <StaticImage
              src="../assets/images/main.jpeg"
              className="hero-img"
              placeholder="blurred"
              layout="fullWidth"
            ></StaticImage>
            <div className="hero-container">
              <div className="hero-text">
                <h1>simply recipes</h1>
                <h4>no fluff, just recipes</h4>
              </div>
            </div>
          </header>
          {/* <FetchData></FetchData> */}
          <AllRecipes></AllRecipes>
        </main>
      </Layout>
    </div>
  )
}
